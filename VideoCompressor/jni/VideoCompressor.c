
#include <stdio.h>
#include <wchar.h>
#include <jni.h>
#include <dlfcn.h>

/*for android logs*/
#include <android/log.h>

#define LOG_TAG "videocompressor_log"
#define LOGI(...) __android_log_print(4, LOG_TAG, __VA_ARGS__);
#define LOGE(...) __android_log_print(6, LOG_TAG, __VA_ARGS__);
//    	System.loadLibrary("avutil-52");
//        System.loadLibrary("avcodec-55");
//        System.loadLibrary("avformat-55");
//        System.loadLibrary("swresample-0");
//        System.loadLibrary("swscale-2");
//        System.loadLibrary("postproc-52");
//        System.loadLibrary("avfilter-4");
void Java_com_lkland_videocompressor_Converter_convert(JNIEnv *env, jobject pObj , jobjectArray paras)
{
	LOGI("begin %d", 1);
	void *handle;
	void *handle1;
	void *handle2;
	void *handle3;
	void *handle4;
	void *handle5;
	void *handle6;
	void *handle7;
	void *handle8;
	void (*Java_com_lkland_videocompressor_Converter_ffmpegrun)(JNIEnv*, jobject, jobjectArray);
	handle1 = dlopen("/data/data/com.lkland.videocompressor/lib/libavutil-52.so", RTLD_LAZY);
	handle2 = dlopen("/data/data/com.lkland.videocompressor/lib/libavcodec-55.so", RTLD_LAZY);
	handle3 = dlopen("/data/data/com.lkland.videocompressor/lib/libavformat-55.so", RTLD_LAZY);
	handle4 = dlopen("/data/data/com.lkland.videocompressor/lib/libswresample-0.so", RTLD_LAZY);
	handle5 = dlopen("/data/data/com.lkland.videocompressor/lib/libswscale-2.so", RTLD_LAZY);
	handle6 = dlopen("/data/data/com.lkland.videocompressor/lib/libpostproc-52.so", RTLD_LAZY);
	handle7 = dlopen("/data/data/com.lkland.videocompressor/lib/libavfilter-4.so", RTLD_LAZY);
	handle8 = dlopen("/data/data/com.lkland.videocompressor/lib/libavfilter-41.so", RTLD_LAZY);
	handle = dlopen("/data/data/com.lkland.videocompressor/lib/libffmpegrun.so", RTLD_LAZY);
	Java_com_lkland_videocompressor_Converter_ffmpegrun= dlsym(handle, "Java_com_lkland_videocompressor_Converter_ffmpegrun");
	(*Java_com_lkland_videocompressor_Converter_ffmpegrun)(env, pObj, paras);
	dlclose(handle);
	dlclose(handle1);
	dlclose(handle2);
	dlclose(handle3);
	dlclose(handle4);
	dlclose(handle5);
	dlclose(handle6);
	dlclose(handle7);
}

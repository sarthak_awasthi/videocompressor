#include <libavcodec/avcodec.h>
#include <libavformat/avformat.h>
#include <libswscale/swscale.h>
#include <libavutil/pixfmt.h>
#include <ffmpeg.c>

#include <stdio.h>
#include <wchar.h>
#include <jni.h>

/*for android logs*/
#include <android/log.h>

#define LOG_TAG "videocompressor_log"
#define LOGI(...) __android_log_print(4, LOG_TAG, __VA_ARGS__);
#define LOGE(...) __android_log_print(6, LOG_TAG, __VA_ARGS__);

void Java_com_lkland_videocompressor_Converter_ffmpegrun(JNIEnv *env, jobject pObj , jobjectArray paras)
{
	LOGI("begin %d", 1);
	int ret = 0;
	int i = 0;
	int argc = 0;
	char **argv = NULL;
	jstring *strr = NULL;

	if (paras != NULL) {
		argc = (*env)->GetArrayLength(env, paras);
		argv = (char **) malloc(sizeof(char *) * argc);
		strr = (jstring *) malloc(sizeof(jstring) * argc);

		for(i=0;i<argc;i++)
		{
			strr[i] = (jstring)(*env)->GetObjectArrayElement(env, paras, i);
			argv[i] = (char *)(*env)->GetStringUTFChars(env, strr[i], 0);
		}
	}
	LOGI("call java method to save frame %d", 1);
	ret = main(argc, argv, env, pObj);

	for(i=0;i<argc;i++)
	{
		(*env)->ReleaseStringUTFChars(env, strr[i], argv[i]);
	}
	free(argv);
	free(strr);
	LOGI("call java method to save frame %d", 2);
}
